### Compilation in Docker
```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://gitlab.gnome.org/GNOME/gtk.git
cd gtk

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

#Z#
apt build-dep -y libgtk-3-dev
apt install -y libgstreamer-plugins-bad1.0-dev libgraphene-1.0-dev git
apt install -y meson ninja-build
apt install -y libvulkan-dev

meson build/

ninja -C build/

```

### QTCreator Includes
```
/usr/include/gtk-3.0/
/usr/include/gdk-pixbuf-2.0/
/usr/include/cairo
/usr/include/gtk-3.0/gdk
/usr/include/gtk-3.0/unix-print/gtk/
/usr/include/gio-unix-2.0/
/usr/include/sysprof-3
/usr/include/glib-2.0
/usr/include/pango-1.0/pango/
/usr/include/rest-0.7/
```

